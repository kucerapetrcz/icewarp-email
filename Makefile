.PHONY: help

help:
	@echo Available commands: enter.php, composer.install, tests, phpstan, phpcs, fix-style

enter.php:
	docker compose exec -u root php sh

composer.install:
	docker compose exec -u root php composer install

tests: phpcs phpstan

phpstan:
	docker compose exec -u root php composer phpstan

phpcs:
	docker compose exec -u root php composer phpcs

fix-style:
	docker compose exec -u root php composer fix-style