SET NAMES utf8mb4;

CREATE TABLE `queue` (
                         `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                         `key` varchar(255) NOT NULL,
                         `subject` varchar(255) NOT NULL,
                         `orderId` varchar(255) NOT NULL,
                         `linkLabel` varchar(255) NOT NULL,
                         `linkUrl` varchar(255) NOT NULL,
                         `date` date NOT NULL,
                         `delayedSend` datetime DEFAULT NULL,
                         `attempts` int(11) NOT NULL DEFAULT 3,
                         `lastAttempt` datetime DEFAULT NULL,
                         `sent` tinyint(4) NOT NULL DEFAULT 0,
                         `lastError` text DEFAULT NULL,
                         PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

CREATE TABLE `address` (
                           `addressId` int(10) unsigned NOT NULL AUTO_INCREMENT,
                           `queueId` int(10) unsigned NOT NULL,
                           `addressType` enum('to','bcc','cc') NOT NULL,
                           `address` varchar(255) NOT NULL,
                           PRIMARY KEY (`addressId`),
                           KEY `queueId` (`queueId`),
                           CONSTRAINT `address_ibfk_1` FOREIGN KEY (`queueId`) REFERENCES `queue` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;



