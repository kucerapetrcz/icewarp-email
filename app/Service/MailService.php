<?php
declare(strict_types=1);

namespace App\Service;

use App\Model\DTO\Queue;
use App\Model\Facade\MailerFacade;
use Nette\Bridges\ApplicationLatte\TemplateFactory;
use Nette\Mail\Mailer;
use Nette\Mail\Message;
use Tracy\ILogger;

class MailService
{

	public function __construct(private readonly TemplateFactory $templateFactory, private readonly Mailer          $mailer, private readonly ILogger         $logger, private readonly MailerFacade    $mailerFacade,)
	{
	}

	public function sendQueue(): void
	{
		foreach ($this->mailerFacade->getQueueData() as $queue) {
			try {
				$this->sendEmail($queue);
				$this->mailerFacade->setSuccess($queue->queueId);
			} catch (\Throwable $e) {
				$this->logger->log($e, ILogger::EXCEPTION);
				$this->mailerFacade->setError($queue->queueId, $e);
			}
		}
	}

	/**
	 * @throws \Exception
	 */
	protected function sendEmail(Queue $queue): void
	{
		$message = match (strtolower($queue->key)) {
			'expiration' => $this->createEmailExpiration($queue),
			default => throw new \Exception('Unsupported template '.$queue->key),
		};
		$this->mailer->send($message);
		$this->logger->log(implode(', ', array_merge($queue->email, $queue->bcc)) . ' - ' . $queue->subject, 'sent-mails-' . $queue->key);
	}

	protected function createEmailExpiration(Queue $queue): Message
	{
		$template = $this->templateFactory->createTemplate();
		$now = new \DateTime();
		$diff = $queue->date->diff(new \DateTime());
//TODO: create proper email template
		$html = $template->renderToString(__DIR__ . '/Templates/Expiration.latte', ['queue' => $queue, 'days' => $diff->d, 'isExpired' => $now > $queue->date]);
		$mail = new Message;
		$mail->setHtmlBody($html);
		$mail->setSubject($queue->subject);
//TODO: add configuration
		$mail->setFrom('info@icewarp.com');
		foreach ($queue->email as $to) {
			$mail->addTo($to);
		}
		foreach ($queue->bcc as $bcc) {
			$mail->addBcc($bcc);
		}

		return $mail;
	}
}
