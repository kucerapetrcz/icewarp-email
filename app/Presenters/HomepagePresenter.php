<?php
declare(strict_types=1);

namespace App\Presenters;

use App\Model\Facade\MailerFacade;
use App\Request\MailRequest;
use Nette;
use Nette\Application\AbortException;

// TODO: Create simple rest presenter and rename
class HomepagePresenter extends Nette\Application\UI\Presenter
{
	/**
	 * @var MailerFacade
	 * @inject
	 */
	public MailerFacade $mailerDb;

	/**
	 * @throws AbortException
	 */
	public function actionDefault(): void
	{
		$body = $this->getHttpRequest()->getRawBody();
		$httpResponse = $this->getHttpResponse();
		try {
			if ($body) {
				$request = new MailRequest($body);
				$id = $this->mailerDb->saveRequest($request);
				$httpResponse->setCode(Nette\Http\IResponse::S202_Accepted);
				$response = ['status' => 'ok', 'id' => $id];
			} else {
				$httpResponse->setCode(Nette\Http\IResponse::S400_BadRequest);
				$response = ['status' => 'error', 'message' => 'Missing body'];
			}
		} catch (\Exception $e) {
			$httpResponse->setCode(Nette\Http\IResponse::S500_InternalServerError);
			$response = ['status' => 'error', 'message' => $e->getMessage()];
		}

		$this->sendJson($response);
	}
}
