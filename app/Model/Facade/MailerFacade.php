<?php
declare(strict_types=1);

namespace App\Model\Facade;

use App\Model\DTO\Queue;
use App\Request\MailRequest;

class MailerFacade
{
	public function __construct(protected \App\Model\MailerModel $mailer)
	{
	}

	/**
	 * @throws \Exception
	 */
	public function saveRequest(MailRequest $request): int
	{
		return $this->mailer->saveRequest($request);
	}

	/**
	 * @return \Generator<Queue>
	 */
	public function getQueueData(): \Generator
	{
		$queueQuery = $this->mailer->getQueue();
		while ($queueData = $queueQuery->fetch()) {
			$queue = new Queue($queueData);
			$address = $this->mailer->getAddress($queue->queueId);
			foreach ($address as $mail) {
				$queue->addAddress($mail);
			}

			yield $queue;
		}
	}

	public function setSuccess(int $queueId): void
	{
		$this->mailer->setSuccess($queueId);
	}

	public function setError(int $queueId, \Throwable|\Exception $e): void
	{
		$this->mailer->setError($queueId, $e);
	}
}
