<?php
declare(strict_types=1);

namespace App\Model\DTO;

use App\Request\Object\Link;
use DateTimeInterface;
use Nette\Database\Row;
use Nette\SmartObject;

class Queue
{
	use SmartObject;
	public int $queueId;
	public string $key;
	public string $subject;
	public string $id;
	public DateTimeInterface $date;
	public Link $link;

	/**
	 * @var string[]
	 */
	public array $email = [];

	/**
	 * @var string[]
	 */
	public array $bcc = [];

	/**
	 * @var string[]
	 */
	public array $cc = [];
	public false|DateTimeInterface $delayed_send = false;
	public function __construct(Row $queueData)
	{
		$this->queueId = $queueData->id;
		$this->key = $queueData->key;
		$this->subject = $queueData->subject;
		$this->id = $queueData->orderId;
		$this->date = $queueData->date;
		$this->link = new Link($queueData->linkLabel, $queueData->linkUrl);
	}

	public function addAddress(Row $mail): void
	{
		if ($mail->addressType === 'to') {
			$this->email[] = $mail->address;
		} elseif ($mail->addressType === 'bcc') {
			$this->bcc[] = $mail->address;
		} elseif ($mail->addressType === 'cc') {
			$this->cc[] = $mail->address;
		}
	}
}
