<?php
declare(strict_types=1);

namespace App\Model;

use App\Request\MailRequest;
use Nette\Database\Connection;
use Nette\Database\ResultSet;
use Nette\Database\Row;

class MailerModel
{
	public function __construct(protected Connection $connection)
	{
	}

	/**
	 * @throws \Exception
	 */
	public function saveRequest(MailRequest $request): int
	{
		$this->connection->beginTransaction();
		try {
			$this->connection->query('INSERT INTO `queue`', [
				'key' => $request->key,
				'subject' => $request->subject,
				'orderId' => $request->id,
				'linkLabel' => $request->link->getLabel(),
				'linkUrl' => $request->link->getUrl(),
				'date' => $request->date,
				'delayedSend' => $request->delayed_send,
			]);
			$id = (int) $this->connection->getInsertId();

			$address = [];
			foreach ($request->email as $to) {
				$address[] = [
					'queueId' => $id,
					'addressType' => 'to',
					'address' => $to
				];
			}
			foreach ($request->bcc as $bcc) {
				$address[] = [
					'queueId' => $id,
					'addressType' => 'bcc',
					'address' => $bcc
				];
			}

			if (count($address) === 0) {
				throw new \Exception('Mail address is missing');
			}

			$this->connection->query('INSERT INTO `address`', $address);
			$this->connection->commit();
		} catch (\Exception $e) {
			$this->connection->rollBack();
			throw $e;
		}
		return $id;
	}

	public function getQueue(): ResultSet
	{
		return $this->connection->query('SELECT `id`, `key`, `subject`, `orderId`, `linkLabel`, `linkUrl`, `date`
            FROM `queue`
            WHERE (`delayedSend` IS NULL OR `delayedSend` <= NOW())
            AND `attempts` > 0
            AND (`lastAttempt` IS NULL OR DATE_ADD(`lastAttempt`, INTERVAL 15 MINUTE) < NOW())
            AND `sent` = 0');
	}

	/**
	 * @param int $queueId
	 * @return Row[]
	 */
	public function getAddress(int $queueId): array
	{
		return $this->connection->fetchAll('SELECT `addressType`, `address` FROM `address` WHERE `queueId`=?', $queueId);
	}

	public function setSuccess(int $queueId): void
	{
		$this->connection->query('UPDATE `queue` SET `sent`=1 WHERE `id`=?', $queueId);
	}

	public function setError(int $queueId, \Throwable|\Exception $e): void
	{
		$this->connection->query('
            UPDATE `queue`
            SET `attempts`=`attempts`-1,
                `lastAttempt`=NOW(),
                `lastError`=?
            WHERE `id`=?', $e->getMessage(), $queueId);
	}
}
