<?php
declare(strict_types=1);

namespace App\Request;

use App\Request\Exceptions\ValidationException;
use App\Request\Object\Link;
use DateTimeInterface;
use Nette\Utils\DateTime;

/**
 * @property-read string $key
 * @property-read string $subject
 * @property-read string $id
 * @property-read DateTimeInterface $date
 * @property-read Link $link
 * @property-read string[] $email
 * @property-read string[] $bcc
 * @property-read false|DateTimeInterface $delayed_send
 */
class MailRequest extends JsonRequest
{
	protected string $key;
	protected string $subject;
	protected string $id;
	protected DateTimeInterface $date;
	protected Link $link;
	/**
	 * @var string[]
	 */
	protected array $email = [];
	/**
	 * @var ?string[]
	 */
	protected ?array $bcc = [];
	protected false|DateTimeInterface $delayed_send = false;


	protected function getRequiredFields(): array
	{
		return [
			'key',
			'subject',
			'id',
			'date',
			'link',
			'email',
		];
	}

	/**
	 * @throws \Exception
	 */
	public function normalize(array $data): array
	{
		if (isset($data['date'])) {
			$data['date'] = DateTime::from($data['date']);
		}
		if (isset($data['id'])) {
			$data['id'] = (string) $data['id'];
		}

		if (isset($data['delayed_send'])
			&& $data['delayed_send'] !== false
			&& strtolower($data['delayed_send']) !== 'false'
		) {
			$data['delayed_send'] = DateTime::from($data['delayed_send']);
		}

		if (isset($data['link'])) {
			if (!isset($data['link']['label'])) {
				throw new ValidationException('Field link.label does not exists in request.');
			}
			if (!isset($data['link']['url'])) {
				throw new ValidationException('Field link.url does not exists in request.');
			}
			$data['link'] = new Link(
				label: $data['link']['label'],
				url: $data['link']['url']
			);
		}

		if (isset($data['email'])) {
			foreach ($data['email'] as $mail) {
				if (!filter_var($mail, FILTER_VALIDATE_EMAIL)) {
					throw new ValidationException('Email '.$mail.' is not valid.');
				}
			}
		}

		if (isset($data['bcc'])) {
			foreach ($data['bcc'] as $mail) {
				if (!filter_var($mail, FILTER_VALIDATE_EMAIL)) {
					throw new ValidationException('Bcc '.$mail.' is not valid.');
				}
			}
		}

		return $data;
	}
}
