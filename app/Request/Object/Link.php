<?php
declare(strict_types=1);

namespace App\Request\Object;

use Nette\SmartObject;

/**
 * @property-read string $label
 * @property-read string $url
 */

class Link
{
	use SmartObject;
	public function __construct(protected string $label, protected string $url,)
	{
	}

	/**
	 * @return string
	 */
	public function getLabel(): string
	{
		return $this->label;
	}

	/**
	 * @return string
	 */
	public function getUrl(): string
	{
		return $this->url;
	}
}
