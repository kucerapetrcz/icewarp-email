<?php
declare(strict_types=1);

namespace App\Request\Exceptions;

use Nette\Application\AbortException;

class TerminateException extends AbortException
{
}
