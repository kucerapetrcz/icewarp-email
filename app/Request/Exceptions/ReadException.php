<?php
declare(strict_types=1);

namespace App\Request\Exceptions;

class ReadException extends ValidationException
{
}
