<?php
declare(strict_types=1);

namespace App\Request;

use App\Request\Exceptions\ParsingException;
use App\Request\Exceptions\ReadException;
use App\Request\Exceptions\ReadOnlyException;
use App\Request\Exceptions\ValidationException;
use Nette\Utils\Json;
use Nette\Utils\JsonException;
use Nette\Utils\Strings;
use TypeError;

abstract class JsonRequest
{
	public function __construct(string $data, bool $strict = false)
	{
		try {
			$this->assign(
				$this->normalize(
					$this->decode($data)
				),
				$strict
			);
		} catch (JsonException $e) {
			throw new ParsingException($e->getMessage(), $e->getCode(), $e);
		} catch (TypeError | ReadOnlyException $e) {
			throw new ValidationException($e->getMessage(), $e->getCode(), $e);
		}
	}

	protected function normalize(array $data): array
	{
		return $data;
	}

	/**
	 * @throws JsonException
	 */
	private function decode(string $content): array
	{
		return (array) Json::decode($content, Json::FORCE_ARRAY);
	}

	/**
	 * @throws ValidationException|TypeError|ReadOnlyException
	 */
	private function assign(array $data, bool $strict): void
	{
		foreach ($this->getRequiredFields() as $field) {
			if (!array_key_exists($field, $data)) {
				throw new ValidationException("Field $field does not exists in message.");
			}

			$this->{$field} = $data[$field];
			unset($data[$field]);
		}

		$this->assignOptional($data, $strict);
	}

	/**
	 * @param array $data
	 * @param bool $strict
	 * @throws ReadOnlyException
	 */
	private function assignOptional(array $data, bool $strict): void
	{
		foreach ($data as $field => $value) {
			try {
				$this->{$field} = $value;
			} catch (ReadOnlyException $e) {
				if ($strict) {
					throw $e;
				}
			}
		}
	}

	/**
	 * @param string $name
	 * @return mixed
	 */
	public function __get(string $name)
	{
		if (property_exists($this, $name)) {
			return $this->$name;
		}
		throw new ReadException("Property {$name} does not exists in class " . __CLASS__);
	}

	/**
	 * @param string $name
	 * @param array $args
	 * @return mixed
	 */
	public function __call(string $name, array $args = [])
	{
		if (str_starts_with($name, 'get') && strlen($name) > 3) {
			$propertyName = Strings::firstLower(substr($name, 3));
			return $this->{$propertyName};
		}
		throw new ReadException('Only get methods allowed.');
	}

	/**
	 * @param string $name
	 * @param mixed $value
	 */
	public function __set(string $name, mixed $value): void
	{
		throw new ReadOnlyException("Can`t set read-only property {$name}.");
	}

	public function __isset(string $name): bool
	{
		return isset($this->$name);
	}

	abstract protected function getRequiredFields(): array;
}
